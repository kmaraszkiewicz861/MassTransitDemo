export class QueueMessage {
  id: string | null = null;
  message: string | null = null;
  createdDateTime: Date | null = null
}
