import { selectQueryMessanges, selectLoading, selectError } from './redux/QueueMessage/queue-messages.selectors';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { QueueMessage } from './models/queue-message';
import { Store } from '@ngrx/store';
import * as QueueMessageActions from './redux/QueueMessage/queue-messages.actions';
import * as MassTransitActions from './redux/MassTransit/mass-transit.actions';
import { PublishRequest } from './models/publish-request';
import { selectPublishRequest } from './redux/MassTransit/mass-transit.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  queueMessages$: Observable<QueueMessage[]> = new Observable<QueueMessage[]>();
  loading$: Observable<boolean> = new Observable<boolean>();
  error$: Observable<any> = new Observable<any>();
  request$: Observable<PublishRequest | null> = new Observable<PublishRequest | null>();

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(QueueMessageActions.loadQueueMessages());

    this.queueMessages$ = this.store.select(selectQueryMessanges);
    this.loading$ = this.store.select(selectLoading);
    this.error$ = this.store.select(selectError);
    this.request$ = this.store.select(selectPublishRequest);
  }

  publishMessage(): void {

    console.log("Test");

    this.store.dispatch(MassTransitActions.setMessageToSend({request: { message: "test" }}));
    this.store.dispatch(MassTransitActions.publishMessage());
  }
}
