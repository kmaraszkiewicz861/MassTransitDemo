import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { QueueMessageReducer } from './redux/QueueMessage/queue-messages.reducer';
import { EffectsModule } from '@ngrx/effects';
import { QueueMessageEffects } from './redux/QueueMessage/queue-messages.effects';
import { massTransitReducer } from './redux/MassTransit/mass-transit.reducer';
import { MassTransitEffects } from './redux/MassTransit/mass-transit.effects';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({
      messages: QueueMessageReducer,
      massTransit: massTransitReducer
    }),
    EffectsModule.forRoot([
      QueueMessageEffects,
      MassTransitEffects
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
