import { createSelector, createFeatureSelector } from '@ngrx/store';
import { MassTransitState } from './mass-transit.reducer';

export const selectMassTransitState = createFeatureSelector<MassTransitState>('massTransit');

export const selectPublishRequest = createSelector(
  selectMassTransitState,
  (state: MassTransitState) => state.request
)
