import { createAction, props } from '@ngrx/store';
import { PublishRequest } from 'src/app/models/publish-request';


export const setMessageToSend = createAction(
  '[MassTransit] set message to send',
  props<{ request: PublishRequest }>()
);

export const publishMessage = createAction(
  '[MassTransit] publish message'
)
