import { createReducer, on } from '@ngrx/store'
import { PublishRequest } from 'src/app/models/publish-request';
import * as MassTransitActions from './mass-transit.actions';

export interface MassTransitState {
  request: PublishRequest | null
}

export const initialState: MassTransitState = {
  request: null
}

export const massTransitReducer = createReducer(
  on(
    MassTransitActions.setMessageToSend,
    (state: MassTransitState, { request }) => ({
      ...state,
      request
    })
  )
)
