import { selectCategoryState } from './../QueueMessage/queue-messages.selectors';
import { map, mergeMap, switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from "@angular/core";
import { MassTransitService } from 'src/app/services/mass-transit.service';
import * as MassTransitActions from './mass-transit.actions';
import { MassTransitState } from './mass-transit.reducer';
import { Store, select } from '@ngrx/store';
import { selectPublishRequest } from './mass-transit.selectors';
import { PublishRequest } from 'src/app/models/publish-request';
import { Observable, firstValueFrom } from 'rxjs';

@Injectable()
export class MassTransitEffects {

  publisMessage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(MassTransitActions.publishMessage),
      ofType<any>(switchMap(action => {
        return this._store.pipe(
          select(selectPublishRequest),
          map((r: PublishRequest | null) => {
            return this._service.publishMessage(r as PublishRequest)
          })
        )
      }))
    )
  )

  constructor(
    private _actions$: Actions,
    private _service: MassTransitService,

    private readonly _store: Store<{ massTransit: MassTransitState}>
  ) { }

}
