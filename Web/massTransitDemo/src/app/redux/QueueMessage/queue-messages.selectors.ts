import { createSelector, createFeatureSelector } from '@ngrx/store';
import { QueueMessageState } from './queue-messages.reducer';

// Select the category state
export const selectCategoryState = createFeatureSelector<QueueMessageState>('messages');

// Select the categories
export const selectQueryMessanges = createSelector(
  selectCategoryState,
  (state: QueueMessageState) => state.messages
);

// Select the loading state
export const selectLoading = createSelector(
  selectCategoryState,
  (state: QueueMessageState) => state.loading
);

// Select the error state
export const selectError = createSelector(
  selectCategoryState,
  (state: QueueMessageState) => state.errors
);
