import { createReducer, on } from '@ngrx/store'
import { QueueMessage } from 'src/app/models/queue-message';
import * as QueueMessageActions from './queue-messages.actions';

export interface QueueMessageState {
  messages: QueueMessage[],
  loading: boolean,
  errors: any
}

export const initialState: QueueMessageState = {
  messages: [],
  loading: true,
  errors: null
}

export const QueueMessageReducer = createReducer(
  initialState,
  on(QueueMessageActions.loadQueueMessages, (state: QueueMessageState) => ({ ...state, loading: true})),
  on(QueueMessageActions.loadQueueMessagesFailure, (state: QueueMessageState, { errors }) => ({
    ...state,
    loading: false,
    errors: errors })),
  on(QueueMessageActions.loadQueueMessagesSuccess, (state: QueueMessageState, { messages }) => ({
    ...state,
    loading: false,
    messages
  }))
)
