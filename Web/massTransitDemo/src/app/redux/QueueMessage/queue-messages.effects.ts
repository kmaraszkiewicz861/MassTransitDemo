import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { QueueMessageService } from 'src/app/services/queue-message.service';
import * as QueueMessageActions from './queue-messages.actions';
import { QueueMessageState } from './queue-messages.reducer';
import { Store } from '@ngrx/store';

@Injectable()
export class QueueMessageEffects {

  loadQueueMessage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(QueueMessageActions.loadQueueMessages),
      mergeMap(() =>
        this._service.getMessages().pipe(
          map((messages) =>
            QueueMessageActions.loadQueueMessagesSuccess({ messages })
          ),
          catchError((error) =>
            of(QueueMessageActions.loadQueueMessagesFailure({ errors: error }))
          )
        )
      )
    )
  );

  constructor(
    private readonly _actions$: Actions,
    private readonly _service: QueueMessageService
  ) { }
}
