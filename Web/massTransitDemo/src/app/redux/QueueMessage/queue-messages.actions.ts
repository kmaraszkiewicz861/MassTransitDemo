import { createAction, props } from '@ngrx/store';
import { QueueMessage } from 'src/app/models/queue-message';

export const loadQueueMessages = createAction('[QueueMessages] Load queue messages');
export const loadQueueMessagesSuccess = createAction(
  '[QueueMessages] load queue messages success',
  props<{ messages: QueueMessage[] }>());
export const loadQueueMessagesFailure = createAction(
  '[QueueMessages] load queue messages failure',
  props<{ errors: any }>()
);
