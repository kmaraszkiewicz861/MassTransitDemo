import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { QueueMessage } from '../models/queue-message';

@Injectable({
  providedIn: 'root'
})
export class QueueMessageService {

  private url = "https://localhost:7065/QueueMessage";

  constructor(private _httpClient: HttpClient) { }

  getMessages(): Observable<QueueMessage[]> {
    return this._httpClient.get<QueueMessage[]>(this.url);
  }
}
