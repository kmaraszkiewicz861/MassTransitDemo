import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PublishRequest } from '../models/publish-request';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MassTransitService {

  private url = "https://localhost:7065/MassTransitDemo";

  constructor(private readonly _httpClient: HttpClient) { }

  publishMessage(request: PublishRequest): Observable<any> {
    return this._httpClient.post(this.url, request);
  }
}
