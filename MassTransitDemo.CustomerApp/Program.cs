﻿// See https://aka.ms/new-console-template for more information
using MassTransit;
using MassTransitDemo.Common.ConfigurationExtensions;
using MassTransitDemo.Common.Consumers;
using MassTransitDemo.Common.Handlers.Commands.Handlers;
using MassTransitDemo.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

CreateHostBuilder(args).Build().Run();

while (true)
{
    await Task.Delay(1000);
}

static IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .ConfigureAppConfiguration((hostingContext, config) =>
        {
            config.SetBasePath(AppDomain.CurrentDomain.BaseDirectory);
            config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
        })
        .ConfigureServices((hostContext, services) =>
        {
            var configuration = hostContext.Configuration;

            services.ConfigureSerilog();
            services.AddScoped<DemoConsumer>();
            services.AddScoped<TestQueueConsumer>();
            services.ConfigureDatabase(configuration);

            services.AddMediatR((MediatRServiceConfiguration config) =>
            {
                config.Lifetime = ServiceLifetime.Scoped;
                config.RegisterServicesFromAssembly(typeof(AddQueueMessageToDbCommandHandler).Assembly);
            });

            services.AddMassTransit(x =>
            {
                x.AddDelayedMessageScheduler();
                x.SetKebabCaseEndpointNameFormatter();

                // By default, sagas are in-memory, but should be changed to a durable
                // saga repository.
                x.SetInMemorySagaRepositoryProvider();

                x.AddConsumer<DemoConsumer>();
                x.AddConsumer<TestQueueConsumer>();

                x.UsingRabbitMq((context, cfg) =>
                {
                    //cfg.Host("rabbitmq");
                    cfg.Host("rabbitmq://localhost:5672");

                    cfg.ReceiveEndpoint("TestDemoQueue", e =>
                    {
                        e.ConfigureConsumer<DemoConsumer>(context);
                        e.ConfigureConsumer<TestQueueConsumer>(context);
                    });

                    cfg.UseDelayedMessageScheduler();
                    cfg.ConfigureEndpoints(context);
                });
            });
        });
