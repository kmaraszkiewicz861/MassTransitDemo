using System.Reflection;
using MassTransit;
using MassTransitDemo.Common.Handlers.Commands.Handlers;
using MassTransitDemo.Common.Publishers;
using MassTransitDemo.Database;

bool isRunningInContainer =
        bool.TryParse(Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER"), out var inContainer) && inContainer;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<IDemoPublisher, DemoPublisher>();
builder.Services.AddMassTransit(x =>
{
    x.AddDelayedMessageScheduler();
    x.SetKebabCaseEndpointNameFormatter();

    // By default, sagas are in-memory, but should be changed to a durable
    // saga repository.
    x.SetInMemorySagaRepositoryProvider();

    x.UsingRabbitMq((ctx, cfg) =>
    {
        if (isRunningInContainer)
        {
            cfg.Host("rabbitmq");
        }
        else
        {
            cfg.Host("rabbitmq://localhost:5672"); // the RabbitMQ address
        }

        cfg.UseDelayedMessageScheduler();
        cfg.ConfigureEndpoints(ctx);
    });
});

builder.Services.ConfigureDatabase(builder.Configuration);
builder.Services.AddMediatR((MediatRServiceConfiguration config) =>
{
    config.Lifetime = ServiceLifetime.Scoped;
    config.RegisterServicesFromAssembly(typeof(AddQueueMessageToDbCommandHandler).Assembly);
});

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(builder => builder.WithOrigins("http://localhost:4200") // replace with your specific origin
        .AllowAnyMethod()
        .AllowAnyHeader());
});

//builder.Services.AddScoped<IConnection>(s => RabbitMQConnectionFactory.CreateConnection());
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

using (IServiceScope scope = app.Services.CreateScope())
{
    scope.DbMigrate();
}

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();
app.UseCors();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();