﻿using MassTransitDemo.Common.Models.Dtos;
using MassTransitDemo.Common.Queries;
using Microsoft.AspNetCore.Mvc;

namespace MassTransitDemo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class QueueMessageController : ControllerBase
    {
        private readonly IQueueMessageQuery _queueMessageQuery;

        public QueueMessageController(IQueueMessageQuery queueMessageQuery)
        {
            _queueMessageQuery = queueMessageQuery;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            IEnumerable<QueueMessageDto> queueMessageDtos = await _queueMessageQuery.GetAllAsync()
                ?? Enumerable.Empty<QueueMessageDto>();

            return Ok(queueMessageDtos);
        }
    }
}