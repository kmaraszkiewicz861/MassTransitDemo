using MassTransitDemo.Common.Models.Dtos;
using MassTransitDemo.Common.Publishers;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace MassTransitDemo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MassTransitDemoController : ControllerBase
    {
        private readonly IDemoPublisher _demoPublisher;

        public MassTransitDemoController(IDemoPublisher demoPublisher)
        {
            _demoPublisher = demoPublisher;
        }

        [HttpPost]
        public async Task<IActionResult> PublishAsync([FromBody] PublishRequest request)
        {
            await _demoPublisher.Publish(request.Message);

            return Ok();
        }
    }
}