﻿using MassTransitDemo.Common.Models.Dtos;
using MassTransitDemo.Common.Queries;
using MassTransitDemo.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace MassTransitDemo.Database.Queries
{
    public class QueueMessageQuery : IQueueMessageQuery
    {
        private readonly QueueDbContext _context;

        public QueueMessageQuery(QueueDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<QueueMessageDto>?> GetAllAsync()
        {
            IEnumerable<QueueMessage> messages = await _context.QueueMessages.AsNoTracking().ToListAsync();

            return messages?.Select(m => new QueueMessageDto(m.Id, m.Message, m.CreatedDateTime));
        }
    }
}
