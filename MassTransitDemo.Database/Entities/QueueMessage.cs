﻿namespace MassTransitDemo.Database.Entities
{
    public class QueueMessage
    {
        public string Id { get; set; }

        public string Message { get; set; }

        public DateTime CreatedDateTime { get; set; }
    }
}
