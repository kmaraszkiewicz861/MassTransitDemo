﻿using MassTransitDemo.Database.Configurations;
using MassTransitDemo.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace MassTransitDemo.Database
{
    public class QueueDbContext : DbContext
    {
        public QueueDbContext()
        {
            
        }

        public QueueDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<QueueMessage> QueueMessages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new QueueMessageConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}