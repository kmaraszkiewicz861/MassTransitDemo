﻿using MassTransitDemo.Database.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MassTransitDemo.Database.Configurations
{
    public class QueueMessageConfiguration : IEntityTypeConfiguration<QueueMessage>
    {
        public void Configure(EntityTypeBuilder<QueueMessage> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Message)
                .HasMaxLength(512)
                .IsRequired();

            builder.Property(x => x.CreatedDateTime)
                .IsRequired();
        }
    }
}
