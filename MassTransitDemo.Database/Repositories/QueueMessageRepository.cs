﻿using MassTransitDemo.Common.Models;
using MassTransitDemo.Common.Models.Dtos;
using MassTransitDemo.Common.Repositories;
using MassTransitDemo.Database.Entities;

namespace MassTransitDemo.Database.Repositories
{
    public class QueueMessageRepository : IQueueMessageRepository
    {
        private readonly QueueDbContext _dbContext;

        public QueueMessageRepository(QueueDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddAsync(QueueMessageDto queueMessage)
        {
            await _dbContext.QueueMessages.AddAsync(new QueueMessage
            {
                Id = queueMessage.Id,
                Message = queueMessage.Message,
                CreatedDateTime = queueMessage.CreatedDateTime
            });
        }
    }
}
