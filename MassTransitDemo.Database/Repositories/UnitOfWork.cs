﻿using MassTransitDemo.Common.Models;
using MassTransitDemo.Common.Repositories;

namespace MassTransitDemo.Database.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly QueueDbContext _dbContext;

        public UnitOfWork(QueueDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Result> SaveChangesAsync()
        {
            try
            {
                await _dbContext.SaveChangesAsync();
                return Result.OkResult();
            }
            catch (Exception ex)
            {
                return Result.FailResult(ex.Message);
            }
        }
    }
}
