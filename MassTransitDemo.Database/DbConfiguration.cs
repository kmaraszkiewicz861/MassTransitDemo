﻿using MassTransitDemo.Common.Queries;
using MassTransitDemo.Common.Repositories;
using MassTransitDemo.Database.Queries;
using MassTransitDemo.Database.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MassTransitDemo.Database
{
    public static class DbConfiguration
    {
        public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration configuration) 
        {
            Console.WriteLine(configuration.GetConnectionString("MyDb"));

            services.AddDbContext<QueueDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("MyDb")));

            services.ConfigureQueries();
            services.ConfigureRepositories();

            return services;
        }

        public static void DbMigrate(this IServiceScope scope)
        {
            var services = scope.ServiceProvider;
            var dbContext = services.GetRequiredService<QueueDbContext>();

            dbContext.Database.Migrate();
        }

        private static void ConfigureQueries(this IServiceCollection services)
        {
            services.AddScoped<IQueueMessageQuery, QueueMessageQuery>();
        }

        private static void ConfigureRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IQueueMessageRepository, QueueMessageRepository>();
        }
    }
}
