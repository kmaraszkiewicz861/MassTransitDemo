﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace MassTransitDemo.Database
{
    public class QueueDbContextFactory : IDesignTimeDbContextFactory<QueueDbContext>
    {
        public QueueDbContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<QueueDbContext>();
            optionsBuilder.UseSqlServer("Server=USER;Database=MassTransitDemo;Trusted_Connection=True;TrustServerCertificate=True;");

            return new QueueDbContext(optionsBuilder.Options);
        }
    }
}