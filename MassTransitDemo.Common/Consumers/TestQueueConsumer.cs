﻿using MassTransit;
using MassTransitDemo.Common.Models;

namespace MassTransitDemo.Common.Consumers
{
    public class TestQueueConsumer : IConsumer<DemoCustomerModel>
    {
        public Task Consume(ConsumeContext<DemoCustomerModel> context)
        {
            DemoCustomerModel model = context.Message;

            Console.WriteLine($"TestQueueConsumer: {model.SendDateTime: yyyy-MM-dd HH:mm:ss}: Id: {model.Id}, Message: {model.Message}");

            return Task.CompletedTask;
        }
    }
}
