﻿using MassTransit;
using MassTransitDemo.Common.Models;

namespace MassTransitDemo.Common.Consumers
{
    public class ErrorMessageConsumer : IConsumer<ErrorQueueMessag>
    {
        public Task Consume(ConsumeContext<ErrorQueueMessag> context)
        {
            Console.WriteLine($"Received error message: {context.Message.Text}");
            return Task.CompletedTask;
        }
    }
}
