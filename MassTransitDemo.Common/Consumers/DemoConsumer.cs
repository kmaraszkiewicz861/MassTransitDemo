﻿using MassTransit;
using MassTransitDemo.Common.Handlers.Commands;
using MassTransitDemo.Common.Models;
using MassTransitDemo.Common.Models.Dtos;
using Microsoft.Extensions.Logging;
using IMediator = MediatR.IMediator;

namespace MassTransitDemo.Common.Consumers
{
    public class DemoConsumer : IConsumer<DemoCustomerModel>
    {
        private readonly IMediator _mediator;

        private readonly ILogger<DemoConsumer> _logger;

        public DemoConsumer(IMediator mediator, ILogger<DemoConsumer> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<DemoCustomerModel> context)
        {
            try
            {
                DemoCustomerModel model = context.Message;
                _logger.LogInformation($"DemoConsumer: {model.SendDateTime: yyyy-MM-dd HH:mm:ss}: Id: {model.Id}, Message: {model.Message}");

                await _mediator.Send(new AddQueueMessageToDbCommand(new QueueMessageDto(model.Id.ToString(), model.Message, model.SendDateTime)));
            }
            catch (Exception ex) 
            {
                _logger.LogError(ex, $"Error occured on class {nameof(DemoConsumer)}");
                Console.WriteLine($"Occured error in {nameof(DemoConsumer)}: {ex}");
            }
        }
    }
}
