﻿using MassTransitDemo.Common.Models.Dtos;

namespace MassTransitDemo.Common.Queries
{
    public interface IQueueMessageQuery
    {
        Task<IEnumerable<QueueMessageDto>?> GetAllAsync();
    }
}
