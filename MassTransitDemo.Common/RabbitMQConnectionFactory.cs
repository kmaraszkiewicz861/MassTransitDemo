﻿using RabbitMQ.Client;

public static class RabbitMQConnectionFactory
{
    public static IConnection CreateConnection()
    {
        var factory = new ConnectionFactory
        {
            HostName = "rabbitmq", // Replace with your RabbitMQ server address
            UserName = "guest", // Replace with your RabbitMQ username
            Password = "guest" // Replace with your RabbitMQ password
        };

        return factory.CreateConnection();
    }
}
