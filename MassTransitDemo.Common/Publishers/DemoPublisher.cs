﻿using MassTransit;
using MassTransitDemo.Common.Models;

namespace MassTransitDemo.Common.Publishers
{
    public class DemoPublisher : IDemoPublisher
    {
        private readonly IBus _bus;

        public DemoPublisher(IBus bus)
        {
            _bus = bus;
        }

        public async Task Publish(string message)
        {
            await _bus.Publish(new DemoCustomerModel(Guid.NewGuid(), message, DateTime.Now));
        }
    }
}
