﻿namespace MassTransitDemo.Common.Publishers
{
    public interface IDemoPublisher
    {
        Task Publish(string message);
    }
}