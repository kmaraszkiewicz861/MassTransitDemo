﻿using MassTransitDemo.Common.Models;
using MassTransitDemo.Common.Models.Dtos;

namespace MassTransitDemo.Common.Repositories
{
    public interface IQueueMessageRepository
    {
        Task AddAsync(QueueMessageDto queueMessage);
    }
}
