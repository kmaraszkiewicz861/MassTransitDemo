﻿using MassTransitDemo.Common.Models;

namespace MassTransitDemo.Common.Repositories
{
    public interface IUnitOfWork
    {
        Task<Result> SaveChangesAsync();
    }
}
