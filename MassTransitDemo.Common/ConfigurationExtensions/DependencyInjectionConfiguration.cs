﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;

namespace MassTransitDemo.Common.ConfigurationExtensions
{
    public static class DependencyInjectionConfiguration
    {
        public static IServiceCollection ConfigureSerilog(this IServiceCollection services)
        {
            services.AddLogging(builder =>
            {
                Logger logger = CreateSerilog();

                builder.ClearProviders();
                builder.AddSerilog(logger);
            });

            return services;
        }

        private static Logger CreateSerilog()
        {
            string logFilePath = Path.Combine(AppContext.BaseDirectory, "logfile.txt");

            return new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File(logFilePath, rollingInterval: RollingInterval.Day)
                .CreateLogger();
        }
    }
}
