﻿namespace MassTransitDemo.Common.Models
{
    public class Result
    {
        public bool IsValid { get; } = true;

        public string ErrorMessage { get; } = string.Empty;

        private Result(string errorMessage)
        {
            ErrorMessage = errorMessage;
            IsValid = false;
        }

        private Result(bool isValid)
        {
            IsValid = isValid;
        }

        private Result()
        {
            IsValid = true;
        }

        public static Result OkResult() => new Result();

        public static Result FailResult(string errorMessage) => new Result(errorMessage);  

        public static Result FailResult() => new Result(false);
    }
}
