﻿namespace MassTransitDemo.Common.Models.Dtos
{
    public record QueueMessageDto(string Id, string Message, DateTime CreatedDateTime);
}
