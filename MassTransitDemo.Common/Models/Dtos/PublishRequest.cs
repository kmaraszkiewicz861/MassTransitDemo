﻿namespace MassTransitDemo.Common.Models.Dtos
{
    public record PublishRequest(string Message);
}
