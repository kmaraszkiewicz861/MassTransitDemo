﻿namespace MassTransitDemo.Common.Models
{
    public record DemoCustomerModel(Guid Id, string Message, DateTime SendDateTime);
}
