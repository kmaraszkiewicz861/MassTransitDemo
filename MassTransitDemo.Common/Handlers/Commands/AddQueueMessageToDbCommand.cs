﻿using MassTransitDemo.Common.Models;
using MassTransitDemo.Common.Models.Dtos;
using MediatR;

namespace MassTransitDemo.Common.Handlers.Commands
{
    public record AddQueueMessageToDbCommand(QueueMessageDto QueueMessage) : IRequest<Result>;
}
