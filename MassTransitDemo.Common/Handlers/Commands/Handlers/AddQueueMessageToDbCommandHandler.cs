﻿using MassTransitDemo.Common.Models;
using MassTransitDemo.Common.Repositories;
using MediatR;

namespace MassTransitDemo.Common.Handlers.Commands.Handlers
{
    public class AddQueueMessageToDbCommandHandler : IRequestHandler<AddQueueMessageToDbCommand, Result>
    {
        private IUnitOfWork _unitOfWork;

        private IQueueMessageRepository _queueMessageRepository;

        public AddQueueMessageToDbCommandHandler(IUnitOfWork unitOfWork, IQueueMessageRepository queueMessageRepository)
        {
            _unitOfWork = unitOfWork;
            _queueMessageRepository = queueMessageRepository;
        }

        public async Task<Result> Handle(AddQueueMessageToDbCommand request, CancellationToken cancellationToken)
        {
            await _queueMessageRepository.AddAsync(request.QueueMessage);
            return await _unitOfWork.SaveChangesAsync();
        }
    }
}
